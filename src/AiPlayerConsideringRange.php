<?php namespace Zahlenraten;

/**
 * Class AiPlayerConsideringRange
 * @package Zahlenraten
 */
class AiPlayerConsideringRange extends AiPlayer
{
    protected int $currentGuess;

    public function guessNumber(): int
    {
        $this->currentGuess = parent::guessNumber();
        return $this->currentGuess;
    }

    public function greaterThanExpectedNumber()
    {
        $this->upperBound = $this->currentGuess - 1;
    }

    public function smallerThanExpectedNumber()
    {
        $this->lowerBound = $this->currentGuess + 1;
    }


}