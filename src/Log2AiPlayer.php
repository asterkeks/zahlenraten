<?php namespace Zahlenraten;

/**
 * Class Log2AiPlayer
 * @package Zahlenraten
 */
class Log2AiPlayer extends AiPlayerConsideringRange
{

    public function guessNumber(): int
    {
        $this->currentGuess = $this->middleOfCurrentRange();
        return $this->currentGuess;
    }

    private function middleOfCurrentRange()
    {
        return $this->lowerBound + $this->halfOfCurrentRange();
    }

    private function halfOfCurrentRange()
    {
        return round(($this->lengthOfCurrentRange() / 2));
    }

    private function lengthOfCurrentRange()
    {
        return $this->upperBound - $this->lowerBound;
    }

}