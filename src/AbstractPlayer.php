<?php namespace Zahlenraten;

/**
 * Class AbstractPlayer
 * @package Zahlenraten
 */
abstract class AbstractPlayer
{

    protected int $lowerBound;
    protected int $upperBound;

    public static function fromRange(int $lowerBound, int $upperBound): self
    {
        $aiPlayer = new static;

        $aiPlayer->lowerBound = $lowerBound;
        $aiPlayer->upperBound = $upperBound;

        return $aiPlayer;
    }

}