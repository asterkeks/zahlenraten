<?php namespace Zahlenraten;

/**
 * Class AiPlayer
 * @package Zahlenraten
 */
class AiPlayer extends AbstractPlayer implements Player
{
    protected int $lowerBound;
    protected int $upperBound;

    public static function forRange(int $lowerBound, int $upperBound): self
    {
    	$aiPlayer = new static;

        $aiPlayer->lowerBound = $lowerBound;
        $aiPlayer->upperBound = $upperBound;

    	return $aiPlayer;
    }

    public function guessNumber(): int
    {
        return random_int($this->lowerBound, $this->upperBound);
    }

    public function smallerThanExpectedNumber()
    {
    }

    public function greaterThanExpectedNumber()
    {
    }

    public function triesUsed(int $tries)
    {
        echo "It took $tries to guess the number";
    }
}