<?php namespace Zahlenraten;

interface Player
{

    public function guessNumber(): int;

    public function smallerThanExpectedNumber();

    public function greaterThanExpectedNumber();

    public function triesUsed(int $tries);
}