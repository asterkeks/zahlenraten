<?php namespace Zahlenraten;

/**
 * Class PlayerFactory
 * @package Zahlenraten
 */
class PlayerFactory
{
    public static function builders() {
        return [
            'ai' => function($lowerBound, $upperBound) { return AiPlayer::fromRange($lowerBound, $upperBound); },
            'smart ai' => function($lowerBound, $upperBound) { return AiPlayerConsideringRange::fromRange($lowerBound, $upperBound); },
            'log2' => function($lowerBound, $upperBound) { return Log2AiPlayer::fromRange($lowerBound, $upperBound); },
            'log2 v2' => function($lowerBound, $upperBound) { return Log2AiPlayer::fromRange($lowerBound, $upperBound); },
        ];
    }

    public static function choices()
    {
        return array_keys(self::builders());
    }

    public static function pickPlayer($pick, $lowerBound, $upperBound) {

        if(array_key_exists($pick, self::builders())) {
            $builder = self::builders();
            return $builder[$pick]($lowerBound, $upperBound);
        }

        return ConsolePlayer::fromRange($lowerBound, $upperBound);
    }

}