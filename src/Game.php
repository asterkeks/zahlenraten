<?php namespace Zahlenraten;

/**
 * Class Game
 * @package Zahlenraten
 */
class Game
{

    private int $expectedNumber;
    private Player $player;

    public function expectNumber(int $expectedNumber)
    {
        $this->expectedNumber = $expectedNumber;
    }

    public function run()
    {
        $tries = 1;
        while( ($guess = $this->player->guessNumber()) !== $this->expectedNumber ) {
            if($guess < $this->expectedNumber) {
                $this->player->smallerThanExpectedNumber();
            }

            if($guess > $this->expectedNumber) {
                $this->player->greaterThanExpectedNumber();
            }

            ++$tries;
        }

        $this->player->triesUsed($tries);
    }

    public function setPlayer(Player $player)
    {
        $this->player = $player;
    }
}