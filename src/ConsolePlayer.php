<?php namespace Zahlenraten;

/**
 * Class ConsolePlayer
 * @package Zahlenraten
 */
class ConsolePlayer extends AbstractPlayer implements Player
{

    public function guessNumber(): int
    {
        return (int)readline("Guess a number between {$this->lowerBound} and {$this->upperBound}: ");
    }

    public function smallerThanExpectedNumber()
    {
        echo "The expected number is greater than that.\n";
    }

    public function greaterThanExpectedNumber()
    {
        echo "The expected number is smaller than that.\n";
    }

    public function triesUsed(int $tries)
    {
        echo "It took you {$tries} tries to guess the number";
    }
}