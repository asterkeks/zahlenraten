<?php

use Zahlenraten\Game;
use Zahlenraten\PlayerFactory;

require_once __DIR__.'/vendor/autoload.php';

function pickPlayer($lowerBound, $upperBound) {
    $result = readline('Play yourself or ai? (SELF/'. implode(',', PlayerFactory::choices()) .'): ');

    return PlayerFactory::pickPlayer($result, $lowerBound, $upperBound);
}

$game = new Game();

$lowerBound = 1;
$upperBound = 100;
$game->setPlayer( pickPlayer($lowerBound, $upperBound) );
$game->expectNumber(random_int($lowerBound, $upperBound));
$game->run();