<?php namespace ZahlenratenTests\Unit;

use Mockery\Mock;
use Zahlenraten\Game;
use Zahlenraten\Player;
use ZahlenratenTests\TestCase;

/**
 * Class GameTest
 * @package ZahlenratenTests\Unit
 */
class GameTest extends TestCase
{
    protected Game $game;
    private \Mockery\LegacyMockInterface|Player|\Mockery\MockInterface $player;

    protected function setUp(): void
    {
        parent::setUp();

        $this->game = new Game;

        $this->player = \Mockery::spy(Player::class);
        $this->game->setPlayer($this->player);
    }


    /**
     * @test
     */
    public function should_end_if_expected_number_is_guessed()
    {
        $this->game->expectNumber(5);
        $this->player->shouldReceive('guessNumber')->once()->andReturn(5);

        $this->game->run();
    }

    /**
     * @test
     */
    public function should_continue_if_wrong_number_is_guessed()
    {
        $this->game->expectNumber(5);
        $this->player->shouldReceive('guessNumber')->once()->andReturn(2);
        $this->player->shouldReceive('guessNumber')->once()->andReturn(5);

        $this->game->run();
    }

    /**
     * @test
     */
    public function should_inform_player_if_guessed_number_is_smaller_than_expected_number()
    {
        $this->game->expectNumber(50);
        $this->player->shouldReceive('guessNumber')->once()->andReturn(25);
        $this->player->shouldReceive('guessNumber')->once()->andReturn(50);

        $this->player->shouldReceive('smallerThanExpectedNumber')->once();

        $this->game->run();
    }

    /**
     * @test
     */
    public function should_inform_player_if_guessed_number_is_greater_than_expected_number()
    {
        $this->game->expectNumber(50);
        $this->player->shouldReceive('guessNumber')->once()->andReturn(75);
        $this->player->shouldReceive('guessNumber')->once()->andReturn(50);

        $this->player->shouldReceive('greaterThanExpectedNumber')->once();

        $this->game->run();
    }

    /**
     * @test
     */
    public function should_inform_player_how_many_tries_were_used()
    {
        $this->game->expectNumber(50);

        $this->player->shouldReceive('guessNumber')->once()->andReturn(75);
        $this->player->shouldReceive('guessNumber')->once()->andReturn(50);

        $this->player->shouldReceive('triesUsed')->once()->with(2);

        $this->game->run();
    }


}